﻿/*
* Copyright Jonathan Duncan 2014

* Dual licensed under the MIT and GPL licenses:
* http://www.opensource.org/licenses/mit-license.php
* http://www.gnu.org/licenses/gpl.html

* Based on jQuery dropper plug-in
*
* Copyright 2010 Scott Trudeau
* 
* Dual licensed under the MIT and GPL licenses:
* http://www.opensource.org/licenses/mit-license.php
* http://www.gnu.org/licenses/gpl.html
*/

function eyedropper() {
    // functions
    var canvasIndexFromEvent, search, stopSearching, startSearching, colorFromData,
        getCanvas, getImageData, getImageDataColor, 
        rgbToHex, toHex, setup, setoffsetObject, setImageData, setupEvents, removeEvents;
    // values
    var eyedropperData;

    this.setupImage = function (imageName, callback, offsetObjectName, zl) {
        eyedropperData = new Object;

        var sourceImage = null;

        if (imageName) {
            sourceImage = document.getElementById(imageName);
            eyedropperData.sourceImage = sourceImage;
        }
        eyedropperData.offsetObject = setoffsetObject(sourceImage, offsetObjectName);

        var canvas = getCanvas(sourceImage);
        setup(callback, zl, canvas);
    };

    this.setupCanvas = function (canvas, callback, offsetObjectName, zl) {
        eyedropperData = new Object;
        eyedropperData.sourceCanvas = canvas;
        eyedropperData.offsetObject = setoffsetObject(canvas, offsetObjectName);

        setup(callback, zl, canvas);
    };
    
    setup = function (callback, zl, canvas) {
        var zoomlevel = 100;
        if (zl) zoomlevel = zl;
        setImageData(canvas);
        eyedropperData.foundcallback = callback;
        startSearching();
        setupEvents();
        eyedropperData.zoomlevel = zoomlevel;
    };

    setImageData = function (canvas) {
        eyedropperData.myData = getImageData(canvas);
        eyedropperData.width = canvas.width;
        eyedropperData.height = canvas.height;
    };
    setoffsetObject = function (obj, ofsetobjName) {

        if (ofsetobjName) {
            return document.getElementById(ofsetobjName);
        } else if (obj) return obj;
        return null;
    };

    setupEvents = function () {
        if (window.addEventListener) {
            document.body.addEventListener("mousedown", stopSearching, false);
            document.body.addEventListener("mousemove", search, false);
            
        } else if (window.attachEvent) {
            document.body.attachEvent("onmousedown", stopSearching);
            document.body.attachEvent("onmousemove", search);
        }
    };
    
    removeEvents = function () {
        if (window.addEventListener) {
            document.body.removeEventListener("mousedown", stopSearching, false);
            document.body.removeEventListener("mousemove", search, false);
           
        } else if (window.attachEvent) {
            document.body.detachEvent("onmousedown", stopSearching);
            document.body.detachEvent("onmousemove", search);
             
        }
    };
    
    stopSearching = function () {
        eyedropperData.searching = false;
        var workarea = $("#workarea");
        workarea.css('cursor', 'auto');
        removeEvents();
    };

    startSearching = function () {
        eyedropperData.searching = true;
    };

    search = function (ev) {

        if (eyedropperData.searching) {
            var color;
          
            color = getImageDataColor(eyedropperData.myData, eyedropperData.width,
            eyedropperData.height, eyedropperData.offsetObject, ev, eyedropperData.zoomlevel);
            if (color) {
                eyedropperData.foundcallback(color);
            };
        }
    };

    getCanvas = function (imgElement) {
        var canvasContext, canvasElement;
        canvasElement = document.createElement("canvas");
        canvasElement.width = imgElement.width;
        canvasElement.height = imgElement.height;
        
        try {
            canvasContext = canvasElement.getContext("2d");
            canvasContext.drawImage(imgElement, 0, 0);
        } catch (error) {
            console.log(error);
            
            return null;
        }
        return canvasElement;
    };

    getImageData = function (cnvs) {
        var canvasContext, imageData;
        try {
            canvasContext = cnvs.getContext("2d");
            imageData = canvasContext.getImageData(0, 0, cnvs.width, cnvs.height);
        } catch (error) {

            console.log(error);
            
            return null;
        }
        return imageData;
    };

    toHex = function (i) {
        var str;
        if (i === undefined) {
            return "FF";
        }
        if (i != null) {
            str = i.toString(16);
        }
        if (str != null) {
            while (str.length < 2) {
                str = "0" + str;
            }
        }
        return str;
    };

    rgbToHex = function (r, g, b) {
        return toHex(r) + toHex(g) + toHex(b);
    };

    colorFromData = function (canvasIndex, data) {
        var color;
        color = {
            r: data[canvasIndex],
            g: data[canvasIndex + 1],
            b: data[canvasIndex + 2],
            alpha: data[canvasIndex + 3]
        };
        
        //when no value
        if (color.r === 0 && color.g === 0 && color.b === 0 && color.alpha === 0) {
            color.r = 255;
            color.g = 255;
            color.b = 255;
        }
        
        color.rgbhex = rgbToHex(color.r, color.g, color.b);
        return color;
    };

    canvasIndexFromEvent = function (e, w, h, offset, zoomlevel) {
        var x, y;
       
        x = e.pageX - parseInt(offset.left);
        y = e.pageY - parseInt(offset.top);

        //Adjust for zooming
        if (zoomlevel == 0) zoomlevel = 100; //don't devidde by 0
        var zoomlevelCoeff = 100 / zoomlevel;
        x = Math.floor(x * zoomlevelCoeff);
        y = Math.floor(y * zoomlevelCoeff);

        if (x >= 0 && x <= w && y >= 0 && y <= h) {
            return (x + y * w) * 4;
        }
        return null;
    };

    getImageDataColor = function (imageData, width, height, offsetObjet, evt, zoomlevel) {
        var canvasIndex, color;
        canvasIndex = canvasIndexFromEvent(evt, width, height, $(offsetObjet).offset(), zoomlevel);
        if (canvasIndex) {
            color = colorFromData(canvasIndex, imageData.data);
            return color;
        }
        return null;
    };
}
