/*
 * ext-paintbrush.js
 *
 * Licensed under the MIT License
 *
 * Copyright(c) 2014 Jonathan Duncan
 *
 */

// Dependencies:
// 1) jQuery
// 2) history.js
// 3) svg_editor.js
// 4) svgcanvas.js

//Set colors on objects from the stroke and fill pickers

svgEditor.addExtension("ext-paintbrush", function (s) {
    var svgCanvas = svgEditor.canvas,
        changeElementCommand = svgedit.history.ChangeElementCommand,
        addToHistory = function (cmd) { svgCanvas.undoMgr.addCommandToHistory(cmd); };
    
    var tool = $('#tool_paintbrush');
    tool.removeClass('disabled');
    return {
        name: 'Extension PaintBrush',
        svgicons: svgEditor.curConfig.extPath + 'paintbrush-icon.xml',
        buttons: [{
            id: 'ext-paintbrush',
            type: 'mode',
            title: 'Paint Brush Tool',
            events: {
                "click": function() {
                    svgCanvas.setMode('ext-paintbrush');
                }
            }
        }],
  
        mouseDown: function (opts) {
            var mode = svgCanvas.getMode();
            if (mode == "ext-paintbrush") {
                var e = opts.event;
                var target = e.target;
                if ($.inArray(target.nodeName, ['svg', 'g', 'use']) == -1) {
                    var changes = {};

                    var change = function (elem, attrname, newvalue) {
                        changes[attrname] = elem.getAttribute(attrname);
                        elem.setAttribute(attrname, newvalue);
                    };

                    var fill = svgCanvas.getColor('fill');
                    var stroke = svgCanvas.getColor('stroke');

                    if (fill) change(target, "fill", fill);
                    if (stroke) change(target, "stroke", stroke);
                   
                    addToHistory(new changeElementCommand(target, changes));
                }
            }
        },
    };
});
