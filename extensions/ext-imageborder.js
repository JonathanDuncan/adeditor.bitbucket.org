/*
 * ext-imageborder.js
 *
 * Licensed under the MIT License
 *
 * Copyright(c) 2014 Jonathan Duncan
 *
 */

/* 
	This is adds a rectangle the same size as the selected image and groups them.
*/
svgEditor.addExtension("imageborder", function (s) {
    var svgCanvas = svgEditor.canvas,
			addToHistory = function (cmd) { svgCanvas.undoMgr.addCommandToHistory(cmd); },
        changeElementCommand = svgedit.history.ChangeElementCommand,
        insertElementCommand = svgedit.history.InsertElementCommand
            ;
    function addBorder(opts) {
       
      
    }

    
    
    return {
        name: "imageborder",
        // For more notes on how to make an icon file, see the source of
        // the hellorworld-icon.xml
        svgicons: "extensions/ext-imageborder.xml",
        // if we have selected an element, grab its paint and enable the eye dropper button
        selectedChanged: addBorder,
        elementChanged: addBorder,
        // Multiple buttons can be added in this array
        buttons: [{
            // Must match the icon ID in helloworld-icon.xml
            id: "tool_imageborder",

            // This indicates that the button will be added to the "mode"
            // button panel on the left side
            type: "mode",

            // Tooltip text
            title: "Add Border to Image",

            // Events
            events: {
                'click': function () {
                    // The action taken when the button is clicked on.
                    // For "mode" buttons, any other button will 
                    // automatically be de-pressed.
                    
                    svgCanvas.setMode("imageborder");
                }
            }
        }],
        // This is triggered when the main mouse button is pressed down 
        // on the editor canvas (not the tool panels)
        mouseDown: function (opts) {
            var e = opts.event;
            var target = e.target;
            
            // if we are not in imageborder mode, we don't want to continue
            var mode = svgCanvas.getMode();
            if (mode != "imageborder") return;

            
            if (target && target.nodeName == ['image']) {


                var changes = {};

                var changeatt = function (elem1, attrname, newvalue) {
                    changes[attrname] = elem1.getAttribute(attrname);
                    elem1.setAttribute(attrname, newvalue);
                };
              
                //get element that is "image"
                var elem = target;
                //set preserveAspectRatio="none" on image 
                changeatt(elem, 'preserveAspectRatio', 'none');
                var batchCmd = new s.BatchCommand();
                batchCmd.addSubCommand(new changeElementCommand(elem, changes));

                //get element location, size
                var x = elem.getAttribute('x');
                var y = elem.getAttribute('y');
                var width = elem.getAttribute("width");
                var height = elem.getAttribute("height");
                //add rectangle 

                var rect = s.addSvgElementFromJson({
                    "element": "rect",
                    "curStyles": true,
                    "attr": {
                        "x": x,
                        "y": y,
                        "width": width,
                        "height": height,
                        "id": s.getNextId(),
                        "opacity": 1,
                        "fill-opacity": 0
                    }
                });
                batchCmd.addSubCommand(new changeElementCommand(elem, changes));
                batchCmd.addSubCommand(new s.InsertElementCommand(rect));
               
                // group image (element) and rectangle
                svgCanvas.clearSelection();
                svgCanvas.addToSelection([elem, rect]);
                svgCanvas.groupSelectedElements();
                
                s.addCommandToHistory(batchCmd);

                svgCanvas.setMode("select");
            }


        },

        // This is triggered from anywhere, but "started" must have been set
        // to true (see above). Note that "opts" is an object with event info
        mouseUp: function (opts) {
           
           
        }
    };
});