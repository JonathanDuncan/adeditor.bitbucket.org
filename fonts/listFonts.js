﻿/*
 * listFonts.js
 *
 * Licensed under the MIT License
 *
 * Copyright(c) 2014 Jonathan Duncan
 * Copyright(c) 2010 Ali Rantakari 
 *
 */

//Check whether Flash is enabled.  If it isn't check list of fonts to see if they are available.
var loadfonts = function () {
    var hasflash = detectflash();

    //If it isn't enabled check list of fonts to see if they are available.
    if (hasflash) loadfromflash();
    else checkpredefinedlistfonts();
};

function detectflash() {
    if (navigator.plugins != null && navigator.plugins.length > 0) {
        return navigator.plugins["Shockwave Flash"] && true;
    }
    if (~navigator.userAgent.toLowerCase().indexOf("webtv")) {
        return true;
    }
    if (~navigator.appVersion.indexOf("MSIE") && !~navigator.userAgent.indexOf("Opera")) {
        try {
            return new ActiveXObject("ShockwaveFlash.ShockwaveFlash") && true;
        } catch (e) { }
    }
    return false;
}

var loadfromflash = function () {
    swfobject.embedSWF("fonts/FontList.swf", "fontsFlashLister", "0", "0", "9.0.0");
};

var checkpredefinedlistfonts = function () {
    fontdetect.setup(); // run setup when the DOM is ready
    var fonts = availableFonts(fontlist());
    setupFonts(fonts);
};

var availableFonts = function (testfonts) {

    var fonts = [];
    for (var i = 0; i < testfonts.length; i++) {
        var fontname = testfonts[i];
        //add available fonts to array.
        if (fontdetect.isInstalled(fontname))
            fonts.push(fontname);
    }
    return fonts;
};

var fontlist = function () {
    return ["Aharoni", "Aldhabi", "Andalus", "Angsana New", "AngsanaUPC", "Antiqua", "Aparajita", "Arabic Typesetting",
        "Arial", "Arial Black", "Batang", "BatangChe", "Bitstream Charter", "Blackletter", "Browallia New", "BrowalliaUPC",
        "Calibri", "Calibri Light", "Cambria", "Cambria Math", "Candara", "Century Schoolbook L", "Comic Sans", "Comic Sans MS",
        "Consolas", "Constantia", "Corbel", "Cordia New", "CordiaUPC", "Courier", "Courier New", "Cursive", "DaunPenh", "David",
        "Decorative", "DejaVu Sans Mono", "DFKai-SB", "DilleniaUPC", "DokChampa", "Dotum", "DotumChe", "Ebrima", "Estrangelo Edessa",
        "EucrosiaUPC", "Euphemia", "FangSong", "Fantasy", "Fraktur", "Franklin Gothic Medium", "FrankRuehl", "FreesiaUPC", "Frosty",
        "Gabriola", "Gadugi", "Garamond", "Gautami", "Geneva", "Georgia", "Gill Sans", "Gisha", "Gulim", "GulimChe", "Gungsuh",
        "GungsuhChe", "Helvetica", "Helvetica Neue", "Impact", "IrisUPC", "Iskoola Pota", "JasmineUPC", "KaiTi", "Kalinga",
        "Kartika", "Khmer UI", "KodchiangUPC", "Kokila", "Lao UI", "Latha", "Leelawadee", "Levenim MT", "LilyUPC", "Lucida Console",
        "Lucida Grande", "Lucida Sans Unicode", "Malgun Gothic", "Mangal", "Marlett", "Meiryo", "Meiryo UI", "Microsoft Himalaya",
        "Microsoft JhengHei", "Microsoft JhengHei UI", "Microsoft New Tai Lue", "Microsoft PhagsPa", "Microsoft Sans Serif",
        "Microsoft Tai Le", "Microsoft Uighur", "Microsoft YaHei", "Microsoft YaHei UI", "Microsoft Yi Baiti", "MingLiU, PMingLiU",
        "MingLiU_HKSCS", "MingLiU_HKSCS-ExtB", "MingLiU-ExtB, PMingLiU-ExtB", "Minion", "Miriam", "Modern", "Monaco",
        "Mongolian Baiti", "Monospace", "MoolBoran", "MS Gothic, MS PGothic", "MS Mincho, MS PMincho", "MS UI Gothic",
        "MV Boli", "Myanmar Text", "Narkisim", "Nimbus Mono L", "Nimbus Sans L", "Nirmala UI", "NSimSun", "Nyala", "Palatino",
        "Palatino Linotype", "Palatino Linotype", "Plantagenet Cherokee", "Raavi", "Rod", "Roman", "Sakkal Majalla", "Sans-serif",
        "Script", "Segoe Print", "Segoe Script", "Segoe UI", "Segoe UI Symbol", "Serif", "Shonar Bangla", "Shruti", "SimHei",
        "SimKai", "Simplified Arabic", "SimSun", "SimSun-ExtB", "Swiss", "Sylfaen", "Sylfaen", "Symbol", "Tahoma", "Times",
        "Times New Roman", "Traditional Arabic", "Trebuchet MS", "Tunga", "Urdu Typesetting", "URW Bookman L", "URW Gothic L",
        "URW Palladio L", "Utsaah", "Vani", "Verdana", "Vijaya", "Vrinda", "Webdings", "Wingdings"];
};

//Called from the flash with the fonts on the system.
function populateFontList(fontArr) {
    setupFonts(fontArr);
}

var setupFonts = function (fontArr) {
    var myFontsHtml = fontListtoLi(fontArr);
    document.getElementById('font_family_dropdown-list').innerHTML = myFontsHtml;

    //Hook up the call back for SVG-Edit
    window.svgEditor.addDropDown('#font_family_dropdown', function () {
        var fam = $(this).text();
        $('#font_family').val($(this).text()).change();
    });
};

//Make list items from font names
var fontListtoLi = function (fontArr) {
    var myFontsHtml = '';
    for (var key in fontArr) {
        var fontName = fontArr[key];

        // trim
        fontName = fontName.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        myFontsHtml += '<li style="font-family: ' + fontName + '">' + fontName + '</li>';
    }
    return myFontsHtml;
};

var fontdetect = (function () {
    var testString = 'mmmmmmmmmwwwwwww';
    var testFont = '"Comic Sans MS"';
    var notInstalledWidth = 0;
    var testbed = null;
    var guid = 0;

    return {
        // must be called when the dom is ready
        setup: function () {
            if ($('#fontInstalledTest').length) return;

            $('head').append('<' + 'style> #fontInstalledTest, #fontTestBed { position: absolute; left: -9999px; top: 0; visibility: hidden; } #fontInstalledTest { font-size: 50px!important; font-family: ' + testFont + ';}</' + 'style>');


            $('body').append('<div id="fontTestBed"></div>').append('<span id="fontInstalledTest" class="fonttest">' + testString + '</span>');
            testbed = $('#fontTestBed');
            notInstalledWidth = $('#fontInstalledTest').width();
        },

        isInstalled: function (font) {
            guid++;

            var style = '<' + 'style id="fonttestStyle"> #fonttest' + guid + ' { font-size: 50px!important; font-family: ' + font + ', ' + testFont + '; } <' + '/style>';

            $('head').find('#fonttestStyle').remove().end().append(style);
            testbed.empty().append('<span id="fonttest' + guid + '" class="fonttest">' + testString + '</span>');

            return (testbed.find('span').width() != notInstalledWidth);
        }
    };

})();

$(window).load(function () {
    // executes when complete page is fully loaded, including all frames, objects and images
    loadfonts();
});